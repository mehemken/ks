# Kubernetes Shortcuts

This is a small tool to save some typing while using `kubectl`. It also has the added benefit of keeping your train of thought moving along, instead of pausing repeatedly over long commands like these.

```
$ kubectl config current-context
$ kubectl config get-contexts | grep foo-bar
$ kubectl config use-context gke-foo-bar-baz-yada-yada-tab-complete
```

`ks` does all the above in one intuitive and easy to remember command.

# Installation

Go to the releases page: [kubernetes shortcuts releases](https://gitlab.com/mehemken/ks/tags)

1. Look for the latest available version.
1. Download the binary for your platform, linux/windows.
1. Add the binary to your PATH. (This may involve moving the binary out of your Downloads folder and into a more permanent directory)

## Setup

If you put your kubeconfig file in a non-default location, you will need to tell `ks` about it. On a linux system the default will be `~/.kube/config`. On a Windows system that might be `%userprofile%/.kube/config`.

Sometimes, however, you might be running a VM on your desktop and have mounted your local filesystem into it. Then you might be using `kubectl` in Ubuntu and also on Windows, both pointing to the same kubeconfig file. That requires customization and it requires that you tell `ks` about it.

The command below points `ks` to a non-default kubeconfig file. You can accomplish the same thing on Windows. For instructions on how to do that Google "set environment variable on windows".

```
➜  export KS_KUBECONFIG="~/.foo/kube.yaml"
```

## Commands

### No command

`ks` will tell you your current context in the most convenient way possible. (Actually that is an inaccurate statement, you can reduce effort by 33% if you alias `ks` to a single letter command. That's 33% because you still need to hit the `<enter>` key. However, both the letter **k** and the letter **s** are on the home row of a US English keyboard. Studies at the highest levels of academia have shown, and continue to show, that such effort is minimal. I strongly suggest you DO NOT alias `ks` to a single letter command.)

```
➜  ks
Current context: minikube
```

### All

You can list all available contexts with the `--all` flag (short option is `-a`). `ks` will prompt you for a choice. You can either enter a number, or hit enter without making a choice. If you make a choice your context will be magically switched. If you make no choice, there will be no change.

```
➜  ks -a
Select a context:
[0] marcellus-wallace
[1] maximus-decimus-meridius
[2] batman
[3] julius-cesar
[4] abraham-lincoln
[5] barak-obama
[6] jennifer-lawrence
[7] minikube

No change.
```

### INPUT

Sometimes you have too many contexts to pick from. You can narrow down the list by giving `ks` a substring of the name of the context you want to use. For example, the sequence "ma" can be found in four of the above conexts. `ks` can show you:

```
➜  ks ma
Select a context:
[0] marcellus-wallace
[1] maximus-decimus-meridius
[2] batman
[3] barak-obama
3
Switched to context "barak-obama".
```
