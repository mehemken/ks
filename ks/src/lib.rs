////////////////////////////////////////
// Extern Crates
////////////////////////////////////////

#[macro_use]
extern crate serde_derive;

extern crate clap;
extern crate dirs;
extern crate serde;
extern crate serde_yaml;


////////////////////////////////////////
// Imports
////////////////////////////////////////

use std::collections::BTreeMap;
use std::env;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::io;
use std::process::Command;
use std::process;


////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////

////////////////////////////////////////
// run defines the logic of the program after the kubeconfig
// has been loaded.
pub fn run(matches: clap::ArgMatches, kubeconfig: Kubeconfig) -> Result<(), Box<Error>> {
    if matches.is_present("list_all") {
        let selection = present_options(&kubeconfig.contexts)?;
        switch_context(selection)?;
        process::exit(0);
    }
    if matches.is_present("INPUT") {
        if let Some(input) = matches.value_of("INPUT") {
            let results = search(input, kubeconfig.contexts);
            let selection = present_options(&results)?;
            switch_context(selection)?;
        }
    } else {
        show_current_context()?;
    };
    Ok(())
}


////////////////////////////////////////
// show_current_context prints the current kubectl context
fn show_current_context() -> Result<(), Box<Error>> {
    let mut cmd = Command::new("kubectl");
    cmd.args(&["config", "current-context"]);
    print!("Current context: ");
    let output = cmd.output()?;
    print!("{}", String::from_utf8_lossy(&output.stdout));
    Ok(())
}


////////////////////////////////////////
// switch_context runs a kubectl command to switch contexts
fn switch_context(context: String) -> Result<(), Box<Error>> {
    let mut cmd = Command::new("kubectl");
    cmd.args(&["config", "use-context", &context]);
    let _status = cmd.status()?;
    Ok(())
}


////////////////////////////////////////
// present_options takes some contexts and prints them out with numerical
// keys for user selection. Returns the selection.
fn present_options(contexts: &Vec<Context>) -> Result<String, Box<Error>> {
    let mut options: BTreeMap<i32, String> = BTreeMap::new();
    for (i, context) in contexts.iter().enumerate() {
        options.insert(i as i32, context.name.clone());
    }
    println!("Select a context:");         // Presenting the options
    for (key, val) in &options {
        println!("[{}] {}", key, val);
    }
    let mut input = String::new();         // Capturing user input
    io::stdin().read_line(&mut input)?;
    input = input.trim().to_string();      // Trim is required for successful parse
    if input.trim().len() == 0 {           // Handling empty input
        println!("No change.");
        process::exit(0);
    }
    let input_i32: i32 = match input.parse() {
        Ok(ok) => ok,
        Err(e) => {
            println!("error parsing '{}': {}", input, e);
            process::exit(1);
        },
    };
    let selected_context = match options.get(&input_i32) {
        Some(v) => v.clone(),
        None    => "not found".to_string(),
    };
    Ok(selected_context)
}


////////////////////////////////////////
// search takes a Vec of contexts and filters them based
// on a search term. Returns a Vec of contexts.
fn search(search_term: &str, contexts: Vec<Context>) -> Vec<Context> {
    let mut results: Vec<Context> = vec![];
    for context in contexts {
        if context.name.contains(search_term) {
            results.push(context);
        }
    }
    if results.len() == 0 {
        println!("No matches. Try: ks --help");   // Handling empty results set
        process::exit(0);
    }
    results
}


////////////////////////////////////////
// expand_user replaces a ~ with /home/<user>, allowing the use of ~
// in the ~/.config/mattock.yaml declarations.
fn expand_user(d: &String) -> Result<String, Box<Error>> {
    let split = match d.find("~") {
        Some(x) => x,
        None    => d.len() + 1,
    };
    if split > d.len() {
        return Ok(d.to_owned())
    }
    let user = env::var("USER")?;
    let home = format!("/home/{}", user);
    Ok(d.replace("~", &home))
}

////////////////////////////////////////////////////////////
// Kubeconfig
////////////////////////////////////////////////////////////

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Kubeconfig {
    pub  contexts:  Vec<Context>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Context {
    pub  name:  String,
}

impl Kubeconfig {
    // Reads ~/.kube/config
    pub fn new() -> Result<Kubeconfig, Box<Error>> {
        let raw_path = match env::var("KUBECONFIG") {
            Ok(ok)  => ok,
            Err(_e) => {
                if cfg!(features="windows") {
                    "%userprofile%/.kube/config".to_string()
                } else {
                    "~/.kube/config".to_string()
                }
            },
        };
        let path = expand_user(&raw_path)?;
        let file = File::open(path)?;
        let mut buf = BufReader::new(file);
        let mut serialized = String::new();
        buf.read_to_string(&mut serialized)?;
        let deserialized: Kubeconfig = serde_yaml::from_str(&serialized)?;
        Ok(deserialized)
    }
}
