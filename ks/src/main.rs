////////////////////////////////////////
// Extern Crates
////////////////////////////////////////

#[macro_use]
extern crate clap;
extern crate ks;


////////////////////////////////////////
// Imports
////////////////////////////////////////

use clap::App;
use ks::{Kubeconfig,run};
use std::process;


////////////////////////////////////////////////////////////
// Main
////////////////////////////////////////////////////////////

fn main() {
    let yaml = load_yaml!("cli.yaml");
    let matches = App::from_yaml(yaml).get_matches();

    let kub = match Kubeconfig::new() {
        Ok(config) => config,
        Err(e)     => {
            println!("Something is off about your config: {}", e);
            process::exit(1);
        }
    };

    if let Err(e) = run(matches, kub) {
        println!("application error: {}", e);
        process::exit(1);
    }

}
